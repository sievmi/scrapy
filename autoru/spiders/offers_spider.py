import scrapy
import json
import autoru
import os


class OffersSpider(scrapy.Spider):
    name = "offers"
    count = 0

    def start_requests(self):
        urls = [str(autoru.generate_url())]

        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        for offer in response.xpath('//h3[@class="listing-item__name"]/a/@href'):
            yield response.follow(offer.extract(), self.parse_offer)

        for href in response.css('.pager__next').css('a::attr(href)'):
            yield response.follow(href, self.parse)

    def parse_offer(self, response):
        data = \
            json.loads(response.css('div.sale-data-attributes::attr(data-bem)').extract_first())['sale-data-attributes']
        self.count += 1
        path = os.path.dirname(autoru.__file__) + '/../results/' + str(self.count) + '/data.json'
        print(path)
        directory = os.path.dirname(path)
        os.mkdir(directory)

        data = {
            'mark': data['mark'],
            'model': data['model'],
            'price': data['price'],
            'year': data['year'],
            'horse_power': data['power']
        }

        with open(path, 'w') as outfile:
            json.dump(data, outfile)
