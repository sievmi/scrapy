from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
from autoru import init_dir

init_dir()

process = CrawlerProcess(get_project_settings())

process.crawl('offers')
process.start()
