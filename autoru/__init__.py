import os
import shutil

def generate_url():
    import json

    with open('../search.json') as data_file:
        data = json.load(data_file)

    url = "https://auto.ru/cars"

    if ('type' in data):
        type_data = data['type']
        if 'mark' in type_data:
            url += "/" + type_data["mark"]
            if ('model' in type_data):
                url += "/" + type_data["model"]

    url += '/all'

    if 'characteristics' in data:
        url += '?'
        characteristics_data = data['characteristics']
        for characteristic in characteristics_data:
            url += characteristic + "=" + characteristics_data[characteristic] + "&"

        url = url[:-1]

    #return url
    return "http://auto.ru/cars/all/"

def results_path():
    return os.path.dirname(__file__) + '/../results/'


def init_dir():
    if os.path.isdir(results_path()):
        print("PIDOR")
        shutil.rmtree(results_path())
        #os.removedirs(os.path.dirname(results_path()))
    os.mkdir(os.path.dirname(results_path()))